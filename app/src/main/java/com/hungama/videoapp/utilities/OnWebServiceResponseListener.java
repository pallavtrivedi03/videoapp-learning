package com.hungama.videoapp.utilities;

public interface OnWebServiceResponseListener {
    void onSuccess(Object result);
    void onFailure(Object result);
}
