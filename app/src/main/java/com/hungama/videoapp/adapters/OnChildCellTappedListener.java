package com.hungama.videoapp.adapters;

import com.hungama.videoapp.models.Node;

public interface OnChildCellTappedListener {
    void onChildCellTappedListener(Node node);
}
