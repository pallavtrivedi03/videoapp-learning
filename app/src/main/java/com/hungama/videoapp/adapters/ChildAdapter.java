package com.hungama.videoapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.hungama.videoapp.R;
import com.hungama.videoapp.models.HomeDatum;

import org.w3c.dom.Node;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ChildViewdHolder> {
    private Context context;
    private Fragment fragment;
    private OnChildCellTappedListener listener;

    List<com.hungama.videoapp.models.Node> nodes;
//    ArrayList nodes = new ArrayList<>(Arrays.asList("Beast","Black Night","Black Panther","Black Widow","Captain America","Falcon","Hank Pym","Hawk Eye","Hercules","Hulk","Iron Man","Mantis","Quick Silver","Scarlet Witch","Swords Man","Thor","Vision","Wasp"));
//    ArrayList images = new ArrayList<>(Arrays.asList(R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph,R.drawable.ph));


    public ChildAdapter(Context context, Fragment fragment, OnChildCellTappedListener listener) {
        this.context = context;
        this.fragment = fragment;
        this.listener = listener;
    }


    public void setData(List<com.hungama.videoapp.models.Node> nodes) {
        this.nodes= nodes;
        this.notifyDataSetChanged();
    }

    @Override
    public ChildViewdHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_recycler_item_view, parent, false);

        ChildViewdHolder viewHolder = new ChildViewdHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChildViewdHolder holder, int position) {
        holder.itemView.setTag(nodes.get(position));

        com.hungama.videoapp.models.Node node = (com.hungama.videoapp.models.Node) nodes.get(position);
        holder.textView.setText(node != null ? node.getTitle() : "");

        RequestOptions options = new RequestOptions();
        options.placeholder(R.drawable.ph)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .error(R.drawable.ph)
                .fitCenter()
                .transform(new RoundedCorners(20));
        Glide
                .with(fragment)
                .load(node!= null ? node.getImageUrl() : "")
                .apply(options)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {

        return nodes != null ? nodes.size() : 0;
    }

    public class ChildViewdHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        public TextView textView;

        public ChildViewdHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.childImageView);
            textView = (TextView) itemView.findViewById(R.id.childTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int itemPosition = getLayoutPosition();
                    com.hungama.videoapp.models.Node node = (com.hungama.videoapp.models.Node) nodes.get(itemPosition);
                    listener.onChildCellTappedListener(node);
                }
            });

        }
    }
}
