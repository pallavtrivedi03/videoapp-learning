package com.hungama.videoapp.adapters;

import com.hungama.videoapp.models.Node;

public interface OnParentCellEventListener {
    void onChildCellTapped(Node node);
}
