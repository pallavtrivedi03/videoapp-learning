package com.hungama.videoapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hungama.videoapp.R;
import com.hungama.videoapp.models.HomeDatum;
import com.hungama.videoapp.models.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParentAdapter extends RecyclerView.Adapter<ParentAdapter.ParentViewHolder> implements OnChildCellTappedListener {
    private Context context;
    private Fragment fragment;
    List<HomeDatum> children;
    RecyclerView.LayoutManager layoutManager;
    ChildAdapter mAdapter;
    OnParentCellEventListener listener;

    public ParentAdapter(Context context, List<HomeDatum> children, Fragment fragment, OnParentCellEventListener listener) {
        this.context = context;
        this.children = children;
        this.fragment = fragment;
        this.listener = listener;
    }

    public void setData(List<HomeDatum> children) {
        this.children = children;
        this.notifyDataSetChanged();
    }

    @Override
    public ParentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_recycler_item_view, parent, false);

        ParentViewHolder viewHolder = new ParentViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ParentViewHolder holder, int position) {
        holder.itemView.setTag(children.get(position));

        HomeDatum childNode = (HomeDatum) children.get(position);
        holder.textView.setText(childNode.getBucketTitle());
        holder.childRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(holder.childRecyclerView.getContext(),LinearLayoutManager.HORIZONTAL,false);

        holder.childRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new ChildAdapter(holder.childRecyclerView.getContext(), this.fragment, this);

        holder.childRecyclerView.setAdapter(mAdapter);
        List<Node> nodes = childNode.getNodes();
        mAdapter.setData(nodes);



    }

    @Override
    public int getItemCount() {

        return children != null ? children.size() : 0;
    }

    public class ParentViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;
        public RecyclerView childRecyclerView;

        public ParentViewHolder(View itemView) {
            super(itemView);

            textView= (TextView) itemView.findViewById(R.id.parentItemTitleView);
            childRecyclerView = (RecyclerView) itemView.findViewById(R.id.childRecyclerView);
        }
    }

    @Override
    public void onChildCellTappedListener(Node node) {
        listener.onChildCellTapped(node);
    }
}
