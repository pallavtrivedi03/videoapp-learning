package com.hungama.videoapp;

import android.app.ActionBar;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.hungama.videoapp.fragments.ContentDetailFragment;
import com.hungama.videoapp.fragments.HomeFragment;
import com.hungama.videoapp.fragments.MovieFragment;
import com.hungama.videoapp.fragments.MusicFragment;
import com.hungama.videoapp.fragments.TVFragment;

public class MainActivity extends AppCompatActivity implements
        HomeFragment.OnFragmentInteractionListener,
        MovieFragment.OnFragmentInteractionListener,
        TVFragment.OnFragmentInteractionListener,
        MusicFragment.OnFragmentInteractionListener,
        ContentDetailFragment.OnFragmentInteractionListener  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);
//        getSupportActionBar().setTitle("Video App");


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        View view =getSupportActionBar().getCustomView();


        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation_view);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                selectedFragment = HomeFragment.newInstance("","");
                                break;
                            case R.id.action_item2:
                                selectedFragment = MovieFragment.newInstance("","");
                                break;
                            case R.id.action_item3:
                                selectedFragment = TVFragment.newInstance("","");
                                break;
                            case R.id.action_item4:
                                selectedFragment = MusicFragment.newInstance("","");
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance("",""));
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
