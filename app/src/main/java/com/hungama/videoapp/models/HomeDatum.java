
package com.hungama.videoapp.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeDatum {

    @SerializedName("bucketTitle")
    @Expose
    private String bucketTitle;
    @SerializedName("nodes")
    @Expose
    private List<Node> nodes = null;

    public String getBucketTitle() {
        return bucketTitle;
    }

    public void setBucketTitle(String bucketTitle) {
        this.bucketTitle = bucketTitle;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

}
